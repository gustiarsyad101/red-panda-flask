#import for post method
from flask import Flask, request, jsonify
import requests

#import for get method
import mariadb

app = Flask(__name__)

# Define your Redpanda endpoint URL
REDPANDA_ENDPOINT = "https://redpanda.com/api/messages"  # Replace with the real Redpanda endpoint URL

@app.route('/send_message', methods=['POST'])
def send_message():
    try:
        # Get the message from the request
        message_data = request.get_json()
        message = message_data.get('message')

        if not message:
            return jsonify({"error": "Message not provided"}), 400

        # Prepare the data for the Redpanda POST request
        data = {"message": message}

        # Send the message to Redpanda
        response = requests.post(REDPANDA_ENDPOINT, json=data)

        if response.status_code == 200:
            return jsonify({"success": "Message sent successfully"}), 200
        else:
            return jsonify({"error": "Failed to send message to Redpanda"}), response.status_code
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    
# API for Get data
# Connect to MariaDB
try:
    conn = mariadb.connect(
        user="root",
        password="Y123Kaca#",
        host="127.0.0.1",
        port=3306,  # Replace with the appropriate port for MariaDB
        database="red_panda",
        autocommit=True
    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cursor = conn.cursor()

@app.route('/data', methods=['GET'])
def get_data():
    try:
        # Execute your SQL query here to fetch data from the database
        # Example query: cursor.execute("SELECT * FROM your_table;")
        # Replace "your_table" with the actual table name you want to read from
        # data = cursor.fetchall()

        # For demonstration purposes, returning a sample response
        data = [{'id': 1, 'name': 'Item 1'}, {'id': 2, 'name': 'Item 2'}]
        return jsonify(data)

    except mariadb.Error as e:
        print(f"Error connecting to MariaDB: {e}")
        return jsonify({'error': 'An error occurred while fetching data.'}), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
